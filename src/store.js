import Vue from 'vue';
import Vuex from 'vuex';
import axios from  './axios-auth';
import globalAxios from 'axios';
import router from  './router'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    idToken:null,
    userId:null,
    user:null
  },
  mutations: {
    authUser(state,userData){
      console.log('userData',userData)
      state.idToken=userData.token;
      state.userId=userData.userId;
      console.log('state.idToken',state.idToken)
    },
    storeUser(state,user){
      console.log('storeUser mutations',user)
      state.user=user;
    },
    logout(state){
      state.idToken=null;
      state.userId=null;
      state.user=null;
    }
     

  },
  actions: {
    autologout({commit},expireTime){
      setTimeout(()=>{commit('logout')},(expireTime *1000 ))

    },
    signup({commit,dispatch},authData){
      console.log(authData)

     
        axios.post('/accounts:signUp?key=AIzaSyA5bYxDHXh0wLFzinyx-iFLlD2ktZolbEI',authData)
          .then(res => {
            console.log(res)
            const now = new Date();
            const expdate= new Date(now.getTime() + (res.data.expiresIn *1000));
            localStorage.setItem('token',res.data.idToken);
            localStorage.setItem('expiresIn',expdate)

            const obj={token:res.data.idToken,userId:res.data.localId,returnSecureToken:true};
            commit('authUser',obj)
            dispatch('storeUser',authData)
            dispatch('autologout',res.data.expiresIn)

          })
          .catch(error => console.log('error',error) )

    },
    login({commit,dispatch},authData){

      authData.returnSecureToken=true;
        axios.post('/accounts:signInWithPassword?key=AIzaSyA5bYxDHXh0wLFzinyx-iFLlD2ktZolbEI',authData)
          .then(res => {
            console.log(res)
            const now = new Date();
            const expdate= new Date(now.getTime() + (res.data.expiresIn *1000));
            localStorage.setItem('token',res.data.idToken);
            localStorage.setItem('expiresIn',expdate)
            const obj={token:res.data.idToken,userId:res.data.localId};
            commit('authUser',obj)
            dispatch('autologout',res.data.expiresIn)
          })
          .catch(error => console.log('error',error) )
       
      
    },
    logout( {commit} ){
      commit('logout');
      router.replace('/signin')
    },
    storeUser({commit,state},userData){
      if(!state.idToken){return}
      let url=`/axios1.json?auth=${state.idToken}`
      console.log('url',url)
      globalAxios.post(url,userData)
      .then(res => console.log(res))
      .catch(error => console.log('error',error) )

    },
    fetchUser({commit,state}){
      if(!state.idToken){return}
      let url=`/axios1.json?auth=${state.idToken}`
      console.log('url',url)
      globalAxios.get(url)
      .then(res => {
        console.log(res)
        const data = res.data
        const users = []
        for (let key in data) {
          const user = data[key]
          user.id = key
          users.push(user)
        }
        console.log(users)
        this.email = users[0].email
        commit('storeUser',users[0])
      })
      .catch(error => console.log(error))
  }

  },
  getters: {
    user(state){
      return state.user;
    },
    getToken(state){
      return state.idToken;
    },
    isAuthenticated(state){
      return state.idToken!==null
    }
  }
})